//
//  KuandoDriver.hpp
//  kuando
//
//  Created by Simon Himmelbauer on 30/12/16.
//  Copyright © 2016 Simon Himmelbauer. All rights reserved.
//

#ifndef KuandoDriver_hpp
#define KuandoDriver_hpp

namespace Kuando
{
#include <libusb-1.0/libusb.h>
    
    class KuandoDriver
    {
    private:
        // PRIVATE FUNCTIONS
        bool isSupported(libusb_device* device);
        void cleanUp();
        
        // PRIVATE VARIABLES
        libusb_device_handle *main_handle = nullptr;
        libusb_context *main_context = nullptr;
        unsigned char endpoint_in = 0;
        unsigned char endpoint_out = 0;
        
    public:
        enum KUANDO_ERROR
        {
            KUANDO_ACCESS_ERROR = -3,           // -> Device is claimed by another process
            KUANDO_NOT_FOUND = -5,              // -> Device is not connected to the host
            KUANDO_DEVICE_CLAIMED = -13,        // -> Device has already been claimed
            KUANDO_DEVICE_NOT_CLAIMED = -14,    // -> Device has not yet been claimed
            KUANDO_TRANSFER_ERROR = -15,
            KUANDO_OTHER_ERROR = -99
        };
        
        
        // PUBLIC FUNCTIONS
        KuandoDriver(int debug_level = 0);
        ~KuandoDriver();
        void claim_device(); // Attempts to open the Kuando Busylight Alpha, if unsuccessful, throws exception
        void push_data(unsigned char* data); // Sends the data package to the device (See the helper class for more information)
        void release_device();
        
    };
}

#endif /* KuandoDriver_hpp */
