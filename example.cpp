//
//  main.cpp
//  kuando
//
//  Created by Simon Himmelbauer on 25/12/16.
//  Copyright © 2016 Simon Himmelbauer. All rights reserved.
//

/*
 err = libusb_control_transfer(handle,
 0x21,
 0x0a,
 0,
 0,
 NULL,
 0,
 0);
*/

#include <iostream>
#include <bitset>
#include "KuandoDriver.hpp"

int main(int argc, const char * argv[]) {
    
    Kuando::KuandoDriver driver;
    driver.claim_device();
    driver.claim_device();
    
    std::bitset<8> ringtone_bit;
    unsigned long i = ringtone_bit.to_ulong();
    unsigned char ringtone = static_cast<unsigned char>(i);
    
    unsigned char *data = new unsigned char[64];
    
    data[0] = 0x10; // cmd: repeat step 0
    data[1] = 3;
    data[2] = 100;
    data[3] = 0;
    data[4] = 0;
    data[5] = 100;
    data[6] = 100;
    data[7] = ringtone;
    
    for (i=8;i<59;i++) data[i]=0;
    for (i=59;i<62;i++) data[i]=0xFF;
    
    
    signed short checksum = 0;
    
    for(i=0;i<62;i++)
        checksum += data[i];
    data[62] = checksum / 256;
    data[63] = checksum % 256;
    
    driver.push_data(data);
    driver.release_device();
    driver.release_device();
    
    return 0;
}
