//
//  KuandoDriver.cpp
//  kuando
//
//  Created by Simon Himmelbauer on 30/12/16.
//  Copyright © 2016 Simon Himmelbauer. All rights reserved.
//

#include "KuandoDriver.hpp"
using namespace Kuando;

// VID and PID of the Kuando Busylight Alpha
#define VENDOR_ID 0x27BB
#define PRODUCT_ID 0x3BCA

// PRIVATE FUNCTIONS

bool KuandoDriver::isSupported(libusb_device *device)
{
    int retVal;
    libusb_device_descriptor desc;
    retVal = libusb_get_device_descriptor(device, &desc);
    if(retVal) return false;
    
    if(desc.idVendor == VENDOR_ID && desc.idProduct == PRODUCT_ID) return true;
    else return false;
    
} // isSupported(libusb_device *device, libusb_device_handle **handle)

void KuandoDriver::cleanUp()
{
    main_handle = nullptr;
    endpoint_out = 0;
    endpoint_in = 0;
}

// PUBLIC FUNCTIONS

KuandoDriver::KuandoDriver(int debug_level)
{
    if(libusb_init(&main_context)) throw KUANDO_ERROR::KUANDO_OTHER_ERROR;
    libusb_set_debug(main_context, debug_level);
}

KuandoDriver::~KuandoDriver()
{
    try {
        release_device();
    } catch (KUANDO_ERROR) {}
    libusb_exit(main_context);
}

void KuandoDriver::claim_device()
{
    // Check if we already have a device
    if(main_handle != nullptr)
    {
        throw KUANDO_ERROR::KUANDO_DEVICE_CLAIMED;
    }
    
    int retVal = 0;
    libusb_device **list;
    ssize_t cnt = libusb_get_device_list(main_context, &list);
    
    for(int i = 0; i < cnt; i++)
    {
        libusb_device *device = list[i];
        if(isSupported(device))
        {
            // Found a compatible device, now try  opening it
            retVal = libusb_open(device, &main_handle);
            if(retVal)
            {
                if( retVal == LIBUSB_ERROR_ACCESS) throw KUANDO_ERROR::KUANDO_ACCESS_ERROR;
                else throw KUANDO_ERROR::KUANDO_OTHER_ERROR;
            }
            libusb_config_descriptor *config = nullptr;
            libusb_get_config_descriptor(device, 0, &config);
            for(uint8_t i = 0; i < config->bNumInterfaces; i++)
            {
                const libusb_interface* interfaceContainer = &config->interface[i];
                for(int j = 0; j < interfaceContainer->num_altsetting; j++)
                {
                    const libusb_interface_descriptor* interface = &interfaceContainer->altsetting[j];
                    for(int k = 0; k < interface->bNumEndpoints; k++)
                    {
                        const libusb_endpoint_descriptor* endpoint = &interface->endpoint[k];
                        if(endpoint->bEndpointAddress & LIBUSB_ENDPOINT_IN) endpoint_in = endpoint->bEndpointAddress;
                        else endpoint_out = endpoint->bEndpointAddress;
                    }
                }
            }
            retVal = libusb_set_auto_detach_kernel_driver(main_handle, 1);
            if(retVal != LIBUSB_ERROR_NOT_SUPPORTED && retVal != LIBUSB_SUCCESS)
            {
                cleanUp();
                libusb_free_device_list(list, 1);
                throw KUANDO_ERROR::KUANDO_OTHER_ERROR;
            }
            retVal = libusb_claim_interface(main_handle, 0);
            if(retVal != 0)
            {
                cleanUp();
                libusb_free_device_list(list, 1);
                throw KUANDO_ERROR::KUANDO_ACCESS_ERROR;
            }
            break;
        }
    }
    if(main_handle == nullptr)
    {
        libusb_free_device_list(list, 1);
        throw KUANDO_ERROR::KUANDO_NOT_FOUND;
    }
    libusb_free_device_list(list, 1);
} // claimDevice()


void KuandoDriver::push_data(unsigned char *data)
{
    // Make sure that the device has already been claimed
    if(main_handle == nullptr)
    {
        throw KUANDO_ERROR::KUANDO_DEVICE_NOT_CLAIMED;
    }
    
    int retVal = 0;
    int arrived = 0;
    
    
    retVal = libusb_interrupt_transfer(main_handle, // libusb_device_handle
                                       endpoint_out,// Endpoint address
                                       data,        // Data
                                       64,          // Size of data
                                       &arrived,    // Arrived bytes
                                       0);          // Timeout
    
    if(retVal != 0 || arrived != 64)
    {
        throw KUANDO_ERROR::KUANDO_TRANSFER_ERROR;
    }
} // push_data(const unsigned char *data)

void KuandoDriver::release_device()
{
    if(main_handle == nullptr)
    {
        throw KUANDO_ERROR::KUANDO_DEVICE_NOT_CLAIMED;
    }
    
    libusb_release_interface(main_handle, 0);
    libusb_close(main_handle);
    cleanUp();
} // release_Device()
