# kuandoDriver
An open source driver for the Kuando Busylight Alpha

Thanks to libusb, it is possible to use the driver on multiple platforms. Simply call the push_data function on the KuandoDriver instance to send the raw data to the device. Refer to the official HID-Documentation of the Kuando Busylight Alpha for constructing the data package, however, a helper class is in development, allowing you to change behaviour of the Busylight Alpha in a more developer-friendly manner.

Currently, the software has only been properly tested on Linux, but it should work on all platforms supported by libusb

# Requirements

-) C++-compiler with C++11 support

-) libusb-1.0.x (Any version should work, however, the latest is always recommended

# Limitations

-) Only one Kuando Busylight Alpha on the same machine is currently supported. kuandoDriver will always claim the first one found

-) Hotplugging has not been tested yet

# Known Issues

-) macOS: macOS automatically claims all HID-devices (including the Kuando Busylight Alpha), which prevents kuandoDriver from claiming exclusive access. There is a workaround by unloading a specific kernel extension, however, this has not been tested yet.

-) Linux: kuandoDriver only works if the running user has read/write-access to the usb device. Depending on your distribution, this might not always be the case. (If your program gets a KUANDO_ACCESS_ERROR, try running it as root in order to see if it is the *root* of the problem.)
